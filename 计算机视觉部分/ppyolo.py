import time
from Detector_ppyolo import Detector, Config
import paddle

model_dir = 'ppyolo_mv3'
config = Config('ppyolo_mv3')
detector = Detector(config, model_dir, use_gpu=True, run_mode='fluid')

def get_object_position_ppyolo(img, min_confidence, extr):
    list_ = []
    confidences = []
    img_crop = []
    result, detections = detector.predict(img, min_confidence)
    #paddle.disable_static()
    for position in detections:
        x = position[2] 
        y = position[3] 
        x1 = position[4]
        y1 = position[5]
        w = x1 - x
        h = y1 - y
        confidence = position[1]
        roi = img[int(y):int(y1), int(x):int(x1)]
        img_crop.append(roi)
        p_p = (x, y, w, h) #(x, y, w, h)
        list_.append(p_p)
        confidences.append(confidence)

    time_ = time.time()
    #extr = Extractor(net)
    if len(img_crop) > 0:
        feature = extr(img_crop)
    else:
        feature = []
    time_extr = time.time()
    print('extr_feature_time:')
    print(time_extr-time_)
    return list_, confidences, feature

def get_object(img, min_confidence):
    list_ = []
    confidences = []
    result, detections = detector.predict(img, min_confidence)
    for position in detections:

        x = position[2] 
        y = position[3] 
        x1 = position[4]
        y1 = position[5]
        w = x1 - x
        h = y1 - y
        confidence = position[1]
        p_p = (x, y, w, h) #(x, y, w, h)
        list_.append(p_p)

    return list_