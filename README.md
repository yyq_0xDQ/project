# 基于龙芯派的无人博物馆解决方案

### 整体介绍

#### 功能简述
1.通过计算机视觉跟踪人物得到位置信息，在龙芯派计算组上对感兴趣的展品进行综合判断，最后在自制物联网耳机上播放对应的展品解说，充当智能导游。

2.输入的信息是人物位置，输入的信息是展品解说，达成人机交互。

3.为了减少延时，和增加可移动性，我们在原先的基础上使用了边缘设备，和分布式计算技术。

#### 项目总架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/120025_35857050_8441239.jpeg "整体.jpg")

### 各模块介绍
#### 龙芯派计算组：
##### ·hardoop架构组成计算组：
利用hardoop架构,以一个龙芯派为主节点，以另一个龙芯派为副节点，组成龙芯计算组，由主节点龙芯派调度任务，进行分布式计算，提高服务端处理能力并保存用户的参观数据。架构图如下
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/124116_7977f245_8441239.png "hadoop.png")

##### ·龙芯计算组总控：
总控由两个龙芯派组成的计算组担任，通过接受自制iot耳机的数据以及参馆人员跟踪（EdgeBoard边缘设备）传回的数据进行耳机与参观人员的匹配，判断参观人员对展品的感兴趣程度，对iot耳机进行信息通信等任务，达到EdgeBoard，龙芯派，iot耳机的联动，实现人机交互的目的。
具体流程如下图 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/124238_2c98dc01_8441239.jpeg "loongson.jpg")

#### 目标跟踪模块：
##### ·项目文件结构： 
下面这张思维导图展示了我们最终的目标跟踪项目的算法思路：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/124618_5b5f3706_8441239.png "1.png")
Figure 1：这是我们整个算法（最终工程项目）的思维导图：包含四个模块，DeepSort 模块、Detector 模块、 
ReID_Extractor 模块以及 Tracking by Detection“总控”模块。 

可以看到，我们训练了三个 ReID 特征提取网络，就是考虑到实际应用是需要尽可能轻量 级、推理速度快的网络。 
理论上来说，ReID 对最终目标跟踪的效果是相对小的：无论是在 Simple Online and Realtime Tracking with a Deep Association Metric 或者是在 FairMOT: On the Fairness of Detection and Re-Identification in Multiple Object Tracking 中，再或者其它的相关论文中，都有直接或间接的实验论证这一点。不同的 re-ID models（特征提取网络），对最终的跟踪效果影响相对较小。
因此，后续继续优化该项目，可以考虑更换更加轻量级的 re-ID models，从而达到 real-time 的实际应用需求。 

##### ·目标检测器： 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/124851_aee7cd8a_8441239.png "2.png")

Figure 2:训练得到 ppyolo detector 的过程


尽管这个过程看起来很简单，但在实际操作的过程中还是会遇到很多的问题的。当然， 我们不仅仅只是训练了这一个目标检测模型，同时我们还训练了使用 MOT20 数据集的 ppyolo 模型和 MOT20 数据集、Crowdhuman 数据集训练的 CenterNet（Free）模型以及在最初实验的时候，我们使用的是 PaddleHub 中使用百度自制的行人数据集训练的 yolo v3 模型。另外，值得一提的是，在最终项目落地的时候，为尽可能的考虑到它的实用性，我们需要将现有的目标检测模型换成更轻量级的目标检测模型：ppyolo-tiny。 


##### ·DeepSort 算法:
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/125024_84e99d38_8441239.png "3.png")

Figure 3：DeepSort 的跟踪流程：1、通过卡尔曼滤波得到检测目标的预测值。 2、计算 。 3、卡尔曼滤波更新。4、 
Association with deep features。 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/125122_db4e0cae_8441239.png "4.png")
Figure 4：四个相应模块，辅助 Tracker 的 update.

#### 耳机制作及工作流程：
##### ·硬件制作：
通过自制PCB，集成了ESP8266和耳机口，可与互联网相连接，小巧，轻便。(PCB见耳机部分文件夹）

##### ·代码编写：
我们使用Arduino对自制耳机进行开发，首先自动连接博物馆Wifi,再与龙芯派计算组进行信息的传递（ID 的匹配，音频的传输等），最后通过播放器播放展品解说。
下图为耳机工作过程
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/125410_298d3eef_8441239.jpeg "iot.jpg")












#### 安装教程

1.  在龙芯派上搭建hadoop,搭建fastapi环境，运行loongson.py
2.  在耳机上搭建arduino环境 运行earphone_code.ino
3.  搭建计算机视觉环境环境 运行Single_camera.py

#### 参与贡献
杨雨桥 韩鹏远 吴昊洋
