#include <ESP8266WiFi.h>
#include <Arduino.h>
#include "AudioFileSourceSPIFFS.h"
#include "AudioGeneratorMP3.h"
#include "AudioOutputI2SNoDAC.h"
#include <FS.h>
/*WiFi信息*/
const char* ssid = "0xDQ";
const char* password = "beijing1065";

/*目标服务器*/
const char* host = "43.132.244.59";/*"www.example.com";*/
const int httpPort = 5555;

/*SPIFFS文件*/
String file_name;// = "/hello.mp3";
File fsUploadFile; 
AudioGeneratorMP3 *mp3;
AudioFileSourceSPIFFS *file;
AudioOutputI2SNoDAC *out;
bool SPIFFS_Flag = 0;
int music_id = 0;
/*IOT设备信息*/
const int ID = 1;
//const int ID = 2;
bool binding_status = 0;
/*函数*/
void led_star(); //led闪烁
/*tcp*/
WiFiClient client;

void setup() {
  /**************初始化**************/
  //引脚初始化
  pinMode(D4, OUTPUT);
  
  //串口初始化 WiFi初始化
  Serial.begin(115200);
  delay(10);
  Serial.print("Connecting to ");
  Serial.println("ssid");
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(". ");  
  }
  Serial.println("");
  Serial.println(WiFi.localIP());

  //SPIFFS初始化
  if(SPIFFS.begin()){                       // 启动闪存文件系统
    Serial.println("SPIFFS Started.");
  } else {
    Serial.println("SPIFFS Failed to Start.");
  }

  
  
}

void loop() {
  /***************发送ID，和状态****************/
  Serial.println("Connecting to");
  Serial.println(host);
 
  /*fsUploadFile = SPIFFS.open("hello.mp3", "w");*/
  /*准备字符串*/
   String url ="/?ID=" + String(ID) 
                + "&Status=" + String(binding_status);
   
  // 建立字符串，用于HTTP请求
  String httpRequest =  String("GET ") + url + " HTTP/1.1\r\n" +
                        "Host: " + host + "\r\n" +
                        "Connection: close\r\n" +
                        "\r\n";
  delay(10);
  Serial.print("Connecting to "); 
  Serial.print(host); 
  //连接服务端
  if (!client.connect(host, httpPort))
  {
    Serial.println("connected failed");
    return;
  }
  else
  {
   Serial.println("connected succeed"); 
  }
 
  //向服务器发送 请求和ID
  client.print(httpRequest); 
  led_star();
  delay(500);
  Serial.println(". ");  
  //读取返回数据
  while( client.available()|| client.connected())
  {
    Serial.println("# "); 
    //判断是否已经绑定 获取绑定状态
    if (client.find("binding_status:")){
      binding_status = client.parseInt();
      Serial.print("binding_status: " ); 
      Serial.println(binding_status);
    }
    //判断是
    if (client.find("music_id:")){
      music_id = client.parseInt();
      if (music_id == 255)
      {
        music_id = 0;
        SPIFFS_Flag = 0;
      }
      Serial.print("music_id: " ); 
      Serial.println(music_id);
    }
    

    //如果已经绑定，则开始等待去接收mp3,存到SPIFFS里
    if (binding_status == 1 && music_id != 0)
    {
      /*闪两次表示服务器绑定成功*/
      led_star();
      led_star();
      /*size_t counti = client.available();
      uint8_t sbuf[counti];
      client.readBytes(sbuf, counti);
      delay(50);
      if(fsUploadFile){
        fsUploadFile.write(sbuf, counti);}
      fsUploadFile.close();*/
      SPIFFS_Flag = 1; 
      
    }
     if (SPIFFS_Flag == 1)
    {
      SPIFFS_Flag = 0;
      file_name = (String)music_id + ".mp3";
      file = new AudioFileSourceSPIFFS("hello.mp3");  
      out = new AudioOutputI2SNoDAC();
      mp3 = new AudioGeneratorMP3();
      
      mp3->begin(file, out);
      digitalWrite(D4, HIGH);
      Serial.print("正在播放第"); 
      Serial.print(music_id); 
      Serial.print("首歌曲\n");
      //播放结束
      if (mp3->isRunning()) {
      if (!mp3->loop()) mp3->stop(); 
      } else {
        Serial.printf("...");
        digitalWrite(D4, LOW); //灯熄灭
        music_id = 0;
        SPIFFS_Flag = 0;
      }
     
    }
  
  }

    /*把数据存到.mp3后开始音频解码*/
 
  
  
}


void led_star()
{
  digitalWrite(D4, LOW);
  delay(500);
  digitalWrite(D4, HIGH);
  delay(500);
}
